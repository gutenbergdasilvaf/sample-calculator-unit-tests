/*
    @AUTHOR: Francisco Gutenberg da Silva Filho
    @EMAIL: gutenbergdasilva@great.ufc.br
    @LINKEDIN: https://www.linkedin.com/in/francisco-gutenberg-da-silva-filho/
*/

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    double a;
    double b;

    @Test
    public void validateAdditionValid(){
        a = 7;
        b = 5;

        Assert.assertEquals(12,Calculator.addition(a,b),0.0);
    }

    @Test
    public void validateSubtractionValid(){
        a = 9;
        b = 4;

        Assert.assertEquals(5,Calculator.subtraction(a,b),0.0);
    }

    @Test
    public void validateMultiplicationValid(){
        a = 7;
        b = 7;

        Assert.assertEquals(49,Calculator.multiplication(a,b),0.0);
    }

    @Test
    public void validateDivisionValid(){
        a = 9;
        b = 3;

        Assert.assertEquals(3,Calculator.division(a,b),0.0);
    }

}
