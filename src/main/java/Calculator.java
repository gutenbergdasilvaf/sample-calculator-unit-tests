public class Calculator {

    /*
        @AUTHOR: Francisco Gutenberg da Silva Filho
        @EMAIL: gutenbergdasilva@great.ufc.br
        @LINKEDIN: https://www.linkedin.com/in/francisco-gutenberg-da-silva-filho/
    */

    public static double addition (double a, double b){
        return a+b;
    }

    public static double subtraction (double a, double b){
        return a-b;
    }

    public static double multiplication (double a, double b){
        return a*b;
    }

    public static double division (double a, double b){
        return a/b;
    }
}
